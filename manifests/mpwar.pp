class { 'apache': }

apache::vhost { 'myMpwar.dev':
port => '80',
docroot => '/web',
docroot_owner => 'vagrant',
docroot_group => 'vagrant',
}

apache::vhost { 'myMpwar.prod':
port => '80',
docroot => '/web',
docroot_owner => 'vagrant',
docroot_group => 'vagrant',
}

$php_version = '55'

# remi_php55 requires the remi repo as well
if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}
# remi_php55 requires the remi repo as well
elsif $php_version == '55' {
  $yum_repo = 'remi-php55'
  ::yum::managed_yumrepo { 'remi-php55':
    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.5',
    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php55/mirror',
    enabled        => 1,
    gpgcheck       => 1,
    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',
    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',
    priority       => 1,
  }
}
# version 5.4
elsif $php_version == '54' {
  $yum_repo = 'remi'
  include ::yum::repo::remi
}

class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

class { "mysql": }

mysql::grant { 'mympwar':
  mysql_privileges => 'ALL',
  mysql_password => '123456',
  mysql_db => 'mympwar',
  mysql_user => 'admin',
  mysql_host => 'host',
}

class { 'yum':
}



